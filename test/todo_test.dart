
import 'package:flutter_test/flutter_test.dart';

import 'package:todo_flutter/util/todo.dart';

void main() {
  test('To do should init default values', () {
    final todo = ToDo();

    final isString = todo.id.runtimeType == String;
    expect(isString,true);

    final isStartInt = todo.startTimestamp.runtimeType == int;
    expect(isStartInt, true);

    final isEndInt = todo.endTimestamp.runtimeType == int;
    expect(isEndInt, true);

    final isBool = todo.isDone.runtimeType == bool;
    expect(isBool, true);

  });
}