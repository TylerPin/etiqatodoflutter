import 'package:flutter/material.dart';
import 'package:todo_flutter/util/todo.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:todo_flutter/pages/todo_addEditRemove_screen.dart';
import 'package:intl/intl.dart';


class Overview extends StatefulWidget {
  static const routeName = '/overview';
  @override
  _OverviewState createState() => _OverviewState();
}

class CustomListItem extends StatelessWidget {
  const CustomListItem({
    this.item,
    this.onTap,
    this.onChanged
  });

  final ToDo item;
  final Function onChanged;
  final Function onTap;
  static const duration = const Duration(seconds: 1);


  @override
  Widget build(BuildContext context) {

    String title = item.title;

    Intl.defaultLocale = "en_US";
    var formatter = new DateFormat('dd/MMMM/yy\nh:mm a');
    DateTime now = DateTime.now();
    DateTime startDate = DateTime.fromMillisecondsSinceEpoch(item.startTimestamp);
    DateTime endDate = DateTime.fromMillisecondsSinceEpoch(item.endTimestamp);
    String startDateStr = formatter.format(startDate);
    String endDateStr = formatter.format(endDate);


    int days = endDate.difference(now).inDays;
    String day = days > 0 ? '${days}d' : "";
    int hours = endDate.difference(now).inHours - endDate.difference(now).inDays * 24;
    String hour = hours > 0 ? '${hours}hr' : "";
    int minutes = endDate.difference(now).inMinutes - endDate.difference(now).inHours * 60;
    String minute = '${minutes}min';

    String timeLeft = endDate.isBefore(now) ? "-" : '${day} ${hour} ${minute}';
    bool isDone = item.isDone;
    String status = isDone? "Complete": "Open";


    return GestureDetector(
      onTap:onTap,
      child: Card(
          child: Column(
            children: <Container> [
              Container(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        title,
                        style: const TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 20.0,
                        ),
                      ),
                    ],
                  ),
                ),
//              color:Colors,yellow,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      flex:1,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0,horizontal: 10),
                        child: Text(
                          "Start",
                          textAlign: TextAlign.start,
                          style: const TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15.0,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex:1,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0,horizontal: 10),
                        child: Text(
                          "End",
                          textAlign: TextAlign.start,
                          style: const TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15.0,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex:1,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0,horizontal: 10),
                        child: Text(
                          "Time left",
                          textAlign: TextAlign.start,
                          style: const TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15.0,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      flex:1,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0,horizontal: 10),
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            startDateStr,
                            textAlign: TextAlign.start,
                            style: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 14.0,
                              height:1.3
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex:1,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0,horizontal: 10),
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            endDateStr,
                            textAlign: TextAlign.start,
                            style: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 14.0,
                                height:1.3
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex:1,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 3.0,horizontal: 10),
                        child: Align(
                          alignment: Alignment.bottomLeft,
                          child: Text(
                            timeLeft,
                            textAlign: TextAlign.start,
                            style: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 14.0,
                                height:1.3
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 0.0,horizontal: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex:1,
                        child: Text(
                          "Status: ${status}",
                          style: const TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 14.0,
                          ),
                        ),
                      ),
                      Text(
                        "Tick if completed",
                        style: const TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 14.0,
                        ),
                      ),
                      Checkbox(
                        value:isDone? true : false,
                        onChanged: onChanged,

                      )
                    ],
                  ),
                ),
                decoration: BoxDecoration(
                  color: Colors.yellow,
                ),
              ),
            ]
          ),
      ),
    );
  }
}

class _OverviewState extends State<Overview> {

  // initial data
  List<ToDo> todoList = [
    ToDo(id:'1589959392002',title:'Get Hired',startTimestamp: 1591320250000,endTimestamp: 1591752250000,isDone: false),
    ToDo(id:'1589959392001',title:'Develop ToDo App',startTimestamp: 1591493050000,endTimestamp: 1591500250000,isDone: true),
    ToDo(id:'1589959392000',title:'Learn Flutter',startTimestamp: 1591410250000,endTimestamp: 1591421050000,isDone: true),
  ];

  @override
  void initState() {
    super.initState();
    _retrieveSavedData();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    _saveData();
  }

  _retrieveSavedData() async {
    final prefs = await SharedPreferences.getInstance();
    final todoStr = prefs.getString('todoStr') ?? jsonEncode(todoList);
    todoList=(json.decode(todoStr) as List).map((i) =>
        ToDo.fromJson(i)).toList();
    todoList.sort((a, b) => b.endTimestamp.compareTo(a.endTimestamp));
  }

  _saveData() async {
    final prefs = await SharedPreferences.getInstance();
    String todoListStr = jsonEncode(todoList);
    prefs.setString('todoStr', todoListStr);
  }

  callbackResult (ToDo val) {
    print("callback received");
    int index = todoList.indexWhere((todo) => todo.id == val.id);
    if (index > -1) {
      List<ToDo> temp = todoList;
      temp[index] = val;
      temp.sort((a, b) => b.endTimestamp.compareTo(a.endTimestamp));
      setState((){
        todoList = temp;
      });
    }
    else {
      List<ToDo> temp = todoList;
      temp.add(val);
      temp.sort((a, b) => b.endTimestamp.compareTo(a.endTimestamp));
      setState((){
        todoList = temp;
      });
    }
    _saveData();
  }

  void _navigateTo([ToDo todo])  {
    if  (todo == null)  {
      todo = ToDo();
    }
    Navigator.push(context,
      MaterialPageRoute(
        builder: (context) => AddEditRemove(todo:todo,callback: callbackResult),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text('To-do list'),
        centerTitle: true,
        elevation: 0,
      ),
      body: ListView.builder(
          itemCount: todoList.length,
          itemBuilder: (context, index) {
            return CustomListItem(
              item:todoList[index],
              onTap:(){
                _navigateTo(todoList[index]);
              },
              onChanged: (bool val){
                setState(() {
                  todoList[index].isDone = val;
                });
              },
            );
          }
      ),

      floatingActionButton:
      FloatingActionButton(
        onPressed: _navigateTo,
        tooltip: 'Add',
        backgroundColor: Colors.blue,
        child: Icon(Icons.add),
      ),
    );
  }
}
