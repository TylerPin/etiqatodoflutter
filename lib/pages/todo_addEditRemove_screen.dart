import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:todo_flutter/util/todo.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AddEditRemove extends StatefulWidget {
  static const routeName = '/addEditRemove';

  ToDo todo;
  Function callback;
  AddEditRemove({Key key, this.todo,this.callback}) : super(key: key);

  @override
  _AddEditRemoveState createState() => _AddEditRemoveState();
}

class _AddEditRemoveState extends State<AddEditRemove> {

  String header = "Add Task";
  var myController = TextEditingController();
  DateTime startDate = DateTime.now();
  DateTime endDate = DateTime.now();

  @override
  void initState() {
    super.initState();
    if (widget.todo.title != "") {
      header = "Edit task";
    }

    myController.text = widget.todo.title;
    startDate =  DateTime.fromMillisecondsSinceEpoch(widget.todo.startTimestamp);
    endDate =  DateTime.fromMillisecondsSinceEpoch(widget.todo.endTimestamp);
  }


  Future<Null> _startDatePicker(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: startDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null)
      setState(() {
        startDate = picked;
        _startTimePicker();
      });
  }
  Future<Null> _endDatePicker(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: endDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null)
      setState(() {
        endDate = picked;
        _endTimePicker();
      });
  }
  Future<Null> _startTimePicker() async {
    TimeOfDay selectedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour:startDate.hour,minute:startDate.minute),
    );
    if (selectedTime == null) return;
    startDate = new DateTime(startDate.year, startDate.month, startDate.day, selectedTime.hour,
        selectedTime.minute);
    setState(() {
      startDate = startDate;
    });
  }
  Future<Null> _endTimePicker() async {
    TimeOfDay selectedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour:endDate.hour,minute:endDate.minute),
    );
    if (selectedTime == null) return;
    endDate = new DateTime(endDate.year, endDate.month, endDate.day, selectedTime.hour,
        selectedTime.minute);
    setState(() {
      endDate = endDate;
    });
  }

  onSave(){
    FocusScope.of(context).requestFocus(FocusNode());
    if (myController.text == "") {
      Fluttertoast.showToast(
          msg: "Please insert a title",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      return;
    }
    if (endDate.isBefore(startDate)) {
      Fluttertoast.showToast(
          msg: "End date must be after start date",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      return;
    }
    widget.todo.title = myController.text;
    widget.todo.startTimestamp = this.startDate.millisecondsSinceEpoch;
    widget.todo.endTimestamp = this.endDate.millisecondsSinceEpoch;
    widget.callback(widget.todo);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {

    Intl.defaultLocale = "en_US";
    var formatter = new DateFormat('dd/MMMM/yyyy h:mm a');
    String startDateStr = formatter.format(startDate);
    String endDateStr = formatter.format(endDate);


    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text(header),
        centerTitle: true,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child:Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget> [
              Text(
                "Title",
                style: const TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0,
                ),
              ),
              TextField(
                controller: myController,
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "Start Date",
                style: const TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0,
                ),
              ),
              RaisedButton(
                onPressed: () => _startDatePicker(context),
                color:Colors.blue,
                child: Text(startDateStr, style: TextStyle(fontSize: 20,color:Colors.white)),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                "End Date",
                style: const TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0,
                ),
              ),
              RaisedButton(
                onPressed: () => _endDatePicker(context),
                color:Colors.blue,
                child: Text(endDateStr, style: TextStyle(fontSize: 20,color:Colors.white)),
              ),
            ]
        ),
      ),

      floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            FloatingActionButton(
              heroTag: "btn2",
              onPressed: onSave,
              tooltip: 'Save',
              child: Icon(Icons.save),
            ),
          ]
      ),
    );
  }

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }
}

