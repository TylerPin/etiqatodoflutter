import 'package:flutter/material.dart';
import 'package:todo_flutter/pages/todo_overview_screen.dart';
import 'package:todo_flutter/pages/todo_addEditRemove_screen.dart';

void main() => runApp(MaterialApp(
    initialRoute: Overview.routeName,
    routes: {
      Overview.routeName: (context) => Overview(),
      AddEditRemove.routeName: (context) => AddEditRemove(),
    }
));

