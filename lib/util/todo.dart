
class ToDo {

  String id; // string timestamp as unique id
  String title;
  int startTimestamp; // timestamp
  int endTimestamp; // timestamp
  bool isDone;

  ToDo({ id, title, startTimestamp,endTimestamp,isDone }){
    this.id = id !=null ? id : new DateTime.now().millisecondsSinceEpoch.toString();
    this.title=title!=null? title: "";
    this.startTimestamp= startTimestamp!=null? startTimestamp:DateTime.now().millisecondsSinceEpoch;
    this.endTimestamp= endTimestamp!=null? endTimestamp:DateTime.now().millisecondsSinceEpoch;
    this.isDone = isDone!=null? isDone: false;
  }

  // Restore from JSON
  factory ToDo.fromJson(dynamic json) {
    return ToDo(id:json['id'] as String,
        title: json['title'] as String,
        startTimestamp: json['startTimestamp'] as int,
        endTimestamp: json['endTimestamp'] as int,
        isDone:json['isDone'] as bool);
  }

  // Convert from JSON
  Map toJson() => {
    'id': id,
    'title': title,
    'startTimestamp': startTimestamp,
    'endTimestamp': endTimestamp,
    'isDone': isDone
  };

}
