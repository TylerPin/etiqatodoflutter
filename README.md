#  To do app

# Setting up Android

---
1. This project assume you have Flutter and Dart setup in your machine, and up and running. (Target flutter version : 1.17.3)
https://flutter.dev/docs/get-started/install
2. Open up your Android studio, and click Open an existing Android Studio project.

![Scheme](./screenshots/openproj.png)

3. Get dependencies, and run the project Android simulator.
4. If you hit Dart SDK error, make sure to add the Dart SDK path according to this answer https://stackoverflow.com/a/51558147/4674872




# Overview

This package consist of 2 screens, Overview and Add/Edit to do list.

### Overview screen
- An overview of all the to do items, sorted by endDate at the top. Time left is dynamimcally calculated everytime a value change.

### Add / Edit screen
* Tapping Add, or any of the to do will bring user to this screen.
* This screen deals with the CRUD operation of our data.

And that's it!

---
# Test Case
* I've added a simple unit test case under /test.

# About Me

I am a React Native developer first and foremost, and it took me a few day's time to pick up flutter. It was fun, and due ecosystem makes  it  very easy to develop Widget based components.



# Screenshots
![Scheme](./screenshots/main.png)
![Scheme](./screenshots/addedit.png)
![Scheme](./screenshots/timepicker.png)